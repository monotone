monotone
========

"In mathematics, a monotonic function (or monotone function) is a function
 between ordered sets that preserves or reverses the given order."
-- Monotonic function, Wikipedia


About
-----

Black-and-white flame fractals synchronized with rich ambient drones.


Build
-----

    git clone https://code.mathr.co.uk/monotone.git
    git clone https://code.mathr.co.uk/clive.git
    cd monotone/src
    make
    ./monotone


Legal
-----

monotone -- flame fractals synchronized with rich ambient drones
Copyright (C) 2016  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
