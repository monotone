#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <assert.h>
#include <complex.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <jack/jack.h>
#include <sndfile.h>

using namespace std;
typedef float sample;
#define SR 48000
#include "dsp.h"

#define FPS 60
#define WIDTH 1920
#define HEIGHT 1080
#define SKIP 1
#define SCALE 1
#define MIPMAP 0
#define QUALITY 12
#define PIPELINE 4
#define LOOP 1
#define STEPS 5
#define SPEED 900

#define GL4 1

typedef struct { HIP hip; LOP lop[2]; } COMPRESS1;

static inline double compress1(COMPRESS1 *s, double hiphz, double lophz1, double lophz2, double db, const double in) {
  double h = hip(&s->hip, in, hiphz);
  h *= h;
  h = lop(&s->lop[0], h, lophz1);
  double env = lop(&s->lop[1], sqrt(fmax(0, h)), lophz2);
  double g = in / env;
  if (isnan(g) || isinf(g)) { g = 0; }
  env = rmstodb(env);
  if (env > db) {
    env = db + (env - db) / 4;
  } else {
    env = db;
  }
  env = 0.25 * dbtorms(env) / dbtorms((100 - db) / 4 + db);
  if (isnan(env) || isinf(env)) { env = 0; }
  return tanh(g * env);
}

struct channel {
  LOP smooth_ds;
  LOP smooth_da;
  PITCHSHIFT shift_up;
  PITCHSHIFT shift_down;
  LOP lop;
  HIP hip;
  DELAY delay;
  float buffer[SR * 2];
};

static struct {
  // graphics
  int which;
  GLuint fbo[5];
  GLuint program[5];
  GLint p0which;
  GLint p0quality;
  GLint p0anim;
  GLint p1which;
  GLint p1quality;
  GLint p3count;
  GLint p3p;
  GLint p3q;
  GLint p4icount;
  GLint p4hcount;
  GLsync syncs[PIPELINE];
  GLuint pbo[PIPELINE + 3];
  // JACK
  jack_client_t  *client;
  jack_port_t *out_port[2];
  // sndfile
  SF_INFO sf_info;
  SNDFILE *sf_file;
  // audio
  double target_ds[4];
  double target_da[4];
  struct channel channels[4];
  COMPRESS1 compressor[4];
} S;

static void process_audio(int nframes, float *l, float *r) {
  for (int c = 0; c < 4; ++c) {
    S.channels[c].delay.length = SR * 2;
  }
  for (int i = 0; i < nframes; ++i) {
    double mixdown[4] = { 0, 0, 0, 0 };
    for (int c = 0; c < 4; ++c) {
      double shift = 6 * lop(&S.channels[c].smooth_da, S.target_da[c], 1);
      double gain = 0.1 * lop(&S.channels[c].smooth_ds, S.target_ds[c], 1);
      double a = delread1(&S.channels[c].delay, 300 + 100 * c) + noise() * 1e-12;
      double up = pitchshift(&S.channels[c].shift_up, 1, 50, shift + 12, a);
      double down = pitchshift(&S.channels[c].shift_down, 1, 50, shift - 24, up);
      a = a + up + down;
      a = tanh((1 + gain) * a/2);
      a = hip(&S.channels[c].hip, lop(&S.channels[c].lop, a, 2000), 50);
      mixdown[c] = a;
    }
    double s = mixdown[0] + mixdown[1] + mixdown[2] + mixdown[3];
    s *= -0.25;
    mixdown[0] += s;
    mixdown[1] += s;
    mixdown[2] += s;
    mixdown[3] += s;
    for (int c = 0; c < 4; ++c) {
      mixdown[c] = compress1(&S.compressor[c], 10, 10, 15, 60, mixdown[c]);
      delwrite(&S.channels[c].delay, mixdown[c]);
    }
    l[i] = 0.5 * (mixdown[0] - mixdown[1]);
    r[i] = 0.5 * (mixdown[2] - mixdown[3]);
  }
}

static int process_callback(jack_nframes_t nframes, void *arg) {
  (void) arg;
  jack_default_audio_sample_t *out[2];
  out[0] = (jack_default_audio_sample_t *) jack_port_get_buffer(S.out_port[0], nframes);
  out[1] = (jack_default_audio_sample_t *) jack_port_get_buffer(S.out_port[1], nframes);
  process_audio(nframes, out[0], out[1]);
  return 0;
}

static void start_audio_realtime(void) {
  // start JACK
  if (! (S.client = jack_client_open("monotone", JackNoStartServer, 0))) {
    fprintf(stderr, "jack server not running?\n");
    exit(1);
  }
  jack_set_process_callback(S.client, process_callback, 0);
  S.out_port[0] = jack_port_register(S.client, "output_1", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
  S.out_port[1] = jack_port_register(S.client, "output_2", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
  if (jack_activate(S.client)) {
    fprintf (stderr, "cannot activate JACK client");
    exit(1);
  }
  if (jack_connect(S.client, "monotone:output_1", "system:playback_1")) {
    fprintf(stderr, "cannot connect output port 1\n");
  }
  if (jack_connect(S.client, "monotone:output_2", "system:playback_2")) {
    fprintf(stderr, "cannot connect output port 2\n");
  }
}

static void start_audio_offline(void) {
  S.sf_info = { 0, 48000, 2, SF_FORMAT_WAV | SF_FORMAT_FLOAT, 0, 0 };
  S.sf_file = sf_open("monotone.wav", SFM_WRITE, &S.sf_info);
}

static void stop_audio_offline(void) {
  sf_close(S.sf_file);
  S.sf_file = 0;
}

static void process_audio_offline(void) {
  float a[2][SR / FPS];
  float b[SR / FPS][2];
  process_audio(SR / FPS, &a[0][0], &a[1][0]);
  for (int i = 0; i < SR / FPS; ++i) {
    for (int j = 0; j < 2; ++j) {
      b[i][j] = a[j][i];
    }
  }
  sf_writef_float(S.sf_file, &b[0][0], SR / FPS);
}

static const char *step_vert =
"#version 330 core\n"
"layout(location = 0) in vec2 pos;\n"
"layout(location = 1) in vec2 tc;\n"
"out vec2 coord;\n"
"void main() {\n"
"  gl_Position = vec4(pos, 0.0, 1.0);\n"
"  coord = vec2(tc.x, 1.0 - tc.y);\n"
"}\n"
;

static const char *step_frag =
"#version 330 core\n"
"uniform sampler2D which;\n"
"uniform int quality;\n"
"uniform mat3x2 anim[4];\n"
"in vec2 coord;\n"
"layout(location = 0) out vec3 colour;\n"
"vec2 fromSquare(vec2 z) {\n"
"  return clamp(atanh(2.0 * z - vec2(1.0)), -4.0, 4.0);\n"
"}"
"vec2 toSquare(vec2 z) {\n"
"  return (tanh(clamp(z, -4.0, 4.0)) + vec2(1.0)) / 2.0;\n"
"}\n"
"float gain(vec2 z, vec2 z0) {\n"
"  vec4 d0 = vec4(dFdx(z), dFdy(z));\n"
"  vec4 d1 = vec4(dFdx(z0), dFdy(z0));\n"
"  return dot(d1,d1) / dot(d0, d0);\n"
"}\n"
"float lookup(vec2 z, vec2 z0) {\n"
"  vec2 w = toSquare(z);\n"
"  return gain(z, z0) * texture(which, w).r;\n"
"}\n"
"void main() {\n"
"  float level = texelFetch(which, ivec2(0,0), quality).r;\n"
"  vec2 z = fromSquare(coord);\n"
"  float total = 0.0;\n"
"  for (int i = 0; i < 4; ++i) {\n"
"    vec2 w = anim[i] * vec3(z, 1.0);\n"
"    total += lookup(w, coord);\n"
"  }\n"
"  float o = clamp(total / level, 0.0, 16777216.0);\n"
"  colour = vec3(1.0, z) * o;\n"
"}\n"
;

static const char *show_vert =
"#version 330 core\n"
"uniform sampler2D which;\n"
"uniform int quality;\n"
"uniform float aspect;\n"
"layout(location = 0) in vec2 pos;\n"
"layout(location = 1) in vec2 tc;\n"
"out vec2 coord;\n"
"void main() {\n"
"  vec3 levels = texelFetch(which, ivec2(0,0), quality).rgb;\n"
"  gl_Position = vec4(pos, 0.0, 1.0);\n"
"  coord = levels.gb / levels.r + vec2((tc.x - 0.5), -(tc.y - 0.5) / aspect);\n"
"}\n"
;

static const char *show_frag =
"#version 330 core\n"
"uniform sampler2D which;\n"
"uniform float level;\n"
"in vec2 coord;\n"
"layout(location = 0) out float colour;\n"
"vec2 toSquare(vec2 z) {\n"
"  return (tanh(clamp(z, -4.0, 4.0)) + vec2(1.0)) / 2.0;\n"
"}\n"
"void main() {\n"
"  colour = texture(which, toSquare(coord)).r;\n"
"}\n"
;

static const char *tone_vert =
"#version 330 core\n"
"uniform vec2 center;\n"
"layout(location = 0) in vec2 pos;\n"
"layout(location = 1) in vec2 tc;\n"
"out vec2 coord;\n"
"void main() {\n"
"  gl_Position = vec4(pos, 0.0, 1.0);\n"
"  coord = vec2(tc.x, 1.0 - tc.y);\n"
"}\n"
;

static const char *tone_frag =
"#version 330 core\n"
"uniform sampler2D source;\n"
"in vec2 coord;\n"
"layout(location = 0) out vec4 colour;\n"
"void main() {\n"
"  colour = vec4(vec3(0.2 * log2(1.0 + 4.0 * log2(1.0 + 4.0 * texture(source, coord).r / texture(source, coord, 1000.0).r))), 1.0);\n"
"}\n"
;

#if GL4

static const char *sort_comp =
"#version 440 core\n"
"layout(local_size_x = 1024) in;\n"
"layout(std430, binding = 0) buffer src {\n"
"  restrict readonly float srcdata[];\n"
"};\n"
"layout(std430, binding = 1) buffer dst {\n"
"  restrict writeonly float dstdata[];\n"
"};\n"
"uniform uint count;\n"
"uniform uint p;\n"
"uniform uint q;\n"
"void main() {\n"
"  uint i = gl_GlobalInvocationID.x;\n"
"  uint d = 1u << (p - q);\n"
"  if ((i & d) == 0 && i < count) {\n"
"    float a = srcdata[i];\n"
"    if ((i | d) < count) {\n"
"      bool up = ((i >> p) & 2) == 0;\n"
"      float b = srcdata[i | d];\n"
"      if ((a > b) == up) {\n"
"        float t = a; a = b; b = t;\n"
"      }\n"
"      dstdata[i | d] = b;\n"
"    }\n"
"    dstdata[i] = a;\n"
"  }\n"
"}\n"
;

static const char *look_comp =
"#version 440 core\n"
"layout(local_size_x = 1024) in;\n"
"layout(std430, binding = 0) buffer img {\n"
"  restrict readonly float imgdata[];\n"
"};\n"
"layout(std430, binding = 1) buffer hst {\n"
"  restrict readonly float hstdata[];\n"
"};\n"
"layout(std430, binding = 2) buffer dst {\n"
"  restrict writeonly float dstdata[];\n"
"};\n"
"uniform uint icount;\n"
"uniform uint hcount;\n"
"void main() {\n"
"  uint i = gl_GlobalInvocationID.x;\n"
"  if (i < icount) {\n"
"    float x = imgdata[i];\n"
"    uint l = 0;\n"
"    uint r = hcount;\n"
"    uint m = (l + r) / 2;\n"
"    for (uint p = 0; p < 24; ++p) {\n"
"      if (r < l + 128) {\n"
"        break;\n"
"      }\n"
"      float y = hstdata[m];\n"
"      if (x < y) {\n"
"        r = m;\n"
"        m = (l + r) / 2;\n"
"        continue;\n"
"      } else if (x > y) {\n"
"        l = m;\n"
"        m = (l + r) / 2;\n"
"        continue;\n"
"      } else {\n"
"       break;\n"
"      }\n"
"    }\n"
"    dstdata[i] = float(m) / float(hcount);\n"
"  }\n"
"}\n"
;

#endif

static void debug_program(GLuint program, const char *name) {
  if (program) {
    GLint linked = GL_FALSE;
    glGetProgramiv(program, GL_LINK_STATUS, &linked);
    if (linked != GL_TRUE) {
      fprintf(stderr, "%s: OpenGL shader program link failed\n", name);
    }
    GLint length = 0;
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
    char *buffer = (char *) malloc(length + 1);
    glGetProgramInfoLog(program, length, 0, buffer);
    buffer[length] = 0;
    if (buffer[0]) {
      fprintf(stderr, "%s: OpenGL shader program info log\n", name);
      fprintf(stderr, "%s\n", buffer);
    }
    free(buffer);
    assert(linked == GL_TRUE);
  } else {
    fprintf(stderr, "%s: OpenGL shader program creation failed\n", name);
  }
}

static void debug_shader(GLuint shader, GLenum type, const char *name) {
  const char *tname = 0;
  switch (type) {
    case GL_VERTEX_SHADER:   tname = "vertex";   break;
    case GL_FRAGMENT_SHADER: tname = "fragment"; break;
    case GL_COMPUTE_SHADER:  tname = "compute";  break;
    default:                 tname = "unknown";  break;
  }
  if (shader) {
    GLint compiled = GL_FALSE;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
    if (compiled != GL_TRUE) {
      fprintf(stderr, "%s: OpenGL %s shader compile failed\n", name, tname);
    }
    GLint length = 0;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
    char *buffer = (char *) malloc(length + 1);
    glGetShaderInfoLog(shader, length, 0, buffer);
    buffer[length] = 0;
    if (buffer[0]) {
      fprintf(stderr, "%s: OpenGL %s shader info log\n", name, tname);
      fprintf(stderr, "%s\n", buffer);
    }
    free(buffer);
    assert(compiled == GL_TRUE);
  } else {
    fprintf(stderr, "%s: OpenGL %s shader creation failed\n", name, tname);
  }
}

static void compile_shader(GLint program, GLenum type, const char *name, const GLchar *source) {
  GLuint shader = glCreateShader(type);
  glShaderSource(shader, 1, &source, 0);
  glCompileShader(shader);
  debug_shader(shader, type, name);
  glAttachShader(program, shader);
  glDeleteShader(shader);
}

static GLint compile_program(const char *name, const GLchar *vert, const GLchar *frag) {
  GLint program = glCreateProgram();
  if (vert) { compile_shader(program, GL_VERTEX_SHADER  , name, vert); }
  if (frag) { compile_shader(program, GL_FRAGMENT_SHADER, name, frag); }
  glLinkProgram(program);
  debug_program(program, name);
  return program;
}

static GLint compile_compute(const char *name, const GLchar *comp) {
  GLint program = glCreateProgram();
  if (comp) { compile_shader(program, GL_COMPUTE_SHADER, name, comp); }
  glLinkProgram(program);
  debug_program(program, name);
  return program;
}

static void key_press_handler(GLFWwindow *window, int key, int scancode, int action, int mods) {
  (void) scancode;
  (void) mods;
  if (action == GLFW_PRESS) {
    switch (key) {
      case GLFW_KEY_Q: case GLFW_KEY_ESCAPE: glfwSetWindowShouldClose(window, GL_TRUE); break;
    }
  }
}

static GLFWwindow *create_window(int major, int minor, int width, int height, const char *title) {
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, major);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, minor);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  glfwWindowHint(GLFW_DECORATED, GL_FALSE);
  GLFWwindow *window = glfwCreateWindow(width, height, title, 0, 0);
  if (! window) {
    fprintf(stderr, "couldn't create window with OpenGL core %d.%d context\n", major, minor);
  }
  assert(window);
  return window;
}

#if GL4
static int debug_error_count = 0;
static void debug_callback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const GLvoid *user) {
  (void) user;
  (void) length;
  const char *source_str = "unknown";
  switch (source) {
    case GL_DEBUG_SOURCE_API:             source_str = "OpenGL";          break;
    case GL_DEBUG_SOURCE_WINDOW_SYSTEM:   source_str = "window system";   break;
    case GL_DEBUG_SOURCE_SHADER_COMPILER: source_str = "shader compiler"; break;
    case GL_DEBUG_SOURCE_THIRD_PARTY:     source_str = "third party";     break;
    case GL_DEBUG_SOURCE_APPLICATION:     source_str = "application";     break;
    case GL_DEBUG_SOURCE_OTHER:           source_str = "application";     break;
  }
  const char *type_str = "unknown";
  switch (type) {
    case GL_DEBUG_TYPE_ERROR:       type_str = "error";       break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: type_str = "deprecated behavior"; break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  type_str = "undefined behavior";  break;
    case GL_DEBUG_TYPE_PORTABILITY: type_str = "portability"; break;
    case GL_DEBUG_TYPE_PERFORMANCE: type_str = "performance"; break;
    case GL_DEBUG_TYPE_MARKER:      type_str = "marker";      break;
    case GL_DEBUG_TYPE_PUSH_GROUP:  type_str = "push group";  break;
    case GL_DEBUG_TYPE_POP_GROUP:   type_str = "pop group";   break;
    case GL_DEBUG_TYPE_OTHER:       type_str = "other";       break;
  }
  const char *severity_str = "unknown";
  switch (severity) {
    case GL_DEBUG_SEVERITY_HIGH:         severity_str = "high";         break;
    case GL_DEBUG_SEVERITY_MEDIUM:       severity_str = "medium";       break;
    case GL_DEBUG_SEVERITY_LOW:          severity_str = "low";          break;
    case GL_DEBUG_SEVERITY_NOTIFICATION: severity_str = "notification"; break;
  }
  bool should_print = true;
  if (should_print) {
    fprintf(stderr, "monotone: %s %s %u %s: %s\n", source_str, type_str, id, severity_str, message);
  }
  if (severity == GL_DEBUG_SEVERITY_HIGH && type == GL_DEBUG_TYPE_ERROR) {
    if (++debug_error_count > 10) {
      abort();
    }
  }
}
#endif

static GLFWwindow *create_context(int width, int height, const char *title) {
  int glfw_initialized = glfwInit();
  if (! glfw_initialized) {
    fprintf(stderr, "couldn't initialize glfw\n");
    assert(glfw_initialized);
    return 0;
  }
  int major, minor;
#if GL4
  GLFWwindow *window = create_window(major = 4, minor = 4, width, height, title);
#else
  GLFWwindow *window = create_window(major = 3, minor = 3, width, height, title);
#endif
  if (! window) {
    fprintf(stderr, "couldn't create window\n");
    assert(window);
    return 0;
  }
  glfwMakeContextCurrent(window);
  glewExperimental = GL_TRUE;
  glewInit();
  glGetError();
#if GL4
  glDebugMessageCallback(debug_callback, 0);
#endif
  return window;
}

static void initialize_gl(int tex_width, int tex_height, int win_width, int win_height) {

  // initialize vertex data for full-screen triangle strip
  GLuint vao;
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);
  GLuint vbo;
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  GLfloat vbo_data[] =
    { -1, -1, 0, 1
    , -1,  1, 0, 0
    ,  1, -1, 1, 1
    ,  1,  1, 1, 0
    };
  glBufferData(GL_ARRAY_BUFFER, 16 * sizeof(GLfloat), vbo_data, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), ((char *)0) + 2 * sizeof(GLfloat));
  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);

  // create textures for ping pong square
  GLuint t_buffer[2];
  glGenTextures(2, t_buffer);
  for (int i = 0; i < 2; ++i) {
    glActiveTexture(GL_TEXTURE0 + i);
    glBindTexture(GL_TEXTURE_2D, t_buffer[i]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, tex_width, tex_height, 0, GL_RGB, GL_FLOAT, 0);
    glGenerateMipmap(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  }

  // create texture for rectangular view
  GLuint t_screen;
  glGenTextures(1, &t_screen);
  glActiveTexture(GL_TEXTURE0 + 2);
  glBindTexture(GL_TEXTURE_2D, t_screen);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, win_width, win_height, 0, GL_RED, GL_FLOAT, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);

  // create framebuffers for ping pong square
  glViewport(0, 0, tex_width, tex_height);
  glClearColor(1, 1, 1, 1);
  glGenFramebuffers(5, S.fbo);
  for (int i = 0; i < 2; ++i) {
    glBindFramebuffer(GL_FRAMEBUFFER, S.fbo[i]);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, t_buffer[1 - i], 0);
    GLenum dbuf = GL_COLOR_ATTACHMENT0;
    glDrawBuffers(1, &dbuf);
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE) {
      fprintf(stderr, "OpenGL framebuffer not complete\n");
    }
    assert(status == GL_FRAMEBUFFER_COMPLETE);
    glClear(GL_COLOR_BUFFER_BIT);
    glActiveTexture(GL_TEXTURE0 + (1 - i));
    glGenerateMipmap(GL_TEXTURE_2D);
  }

  // create framebuffer for rectangular view
  glBindFramebuffer(GL_FRAMEBUFFER, S.fbo[2]);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, t_screen, 0);
  GLenum dbuf = GL_COLOR_ATTACHMENT0;
  glDrawBuffers(1, &dbuf);
  GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
  if (status != GL_FRAMEBUFFER_COMPLETE) {
    fprintf(stderr, "OpenGL framebuffer not complete\n");
  }
  assert(status == GL_FRAMEBUFFER_COMPLETE);

  // compile shaders
  S.program[0] = compile_program("step", step_vert, step_frag);
  S.p0which = glGetUniformLocation(S.program[0], "which");
  S.p0quality = glGetUniformLocation(S.program[0], "quality");
  S.p0anim  = glGetUniformLocation(S.program[0], "anim");
  S.program[1] = compile_program("show", show_vert, show_frag);
  glUseProgram(S.program[1]);
  S.p1which = glGetUniformLocation(S.program[1], "which");
  S.p1quality = glGetUniformLocation(S.program[1], "quality");
  GLuint aspect = glGetUniformLocation(S.program[1], "aspect");
  glUniform1f(aspect, WIDTH / (float) HEIGHT);
  S.which = 0;
  S.program[2] = compile_program("tone", tone_vert, tone_frag);
  glUseProgram(S.program[2]);
  GLint p2source = glGetUniformLocation(S.program[2], "source");
  glUniform1i(p2source, 2);

  // pixel buffer pipeline for image readback
  glGenBuffers(PIPELINE, &S.pbo[0]);
  for (int i = 0; i < PIPELINE; ++i) {
    glBindBuffer(GL_PIXEL_PACK_BUFFER, S.pbo[i]);
    glBufferStorage(GL_PIXEL_PACK_BUFFER, win_width * win_height * sizeof(float), 0, GL_MAP_READ_BIT);
  }

#if GL4
  // pixel buffers for ping pong histogram equalisation
  int logsize = ceil(log2(win_width * win_height));
  glGenBuffers(3, &S.pbo[PIPELINE]);
  for (int i = 0; i < 3; ++i) {
    glBindBuffer(GL_PIXEL_PACK_BUFFER, S.pbo[PIPELINE + i]);
    glBufferData(GL_PIXEL_PACK_BUFFER, (1 << logsize) * sizeof(float), 0, GL_DYNAMIC_COPY);
  }

  // compute shaders
  S.program[3] = compile_compute("sort", sort_comp);
  S.p3count = glGetUniformLocation(S.program[3], "count");
  S.p3p = glGetUniformLocation(S.program[3], "p");
  S.p3q = glGetUniformLocation(S.program[3], "q");
  S.program[4] = compile_compute("look", look_comp);
  S.p4icount = glGetUniformLocation(S.program[4], "icount");
  S.p4hcount = glGetUniformLocation(S.program[4], "hcount");

#endif

}

struct affine {
  double r, a, x, y;
};

struct mat3x2 {
  GLfloat m[6];
}; 

static double cubic(double a, double b, double c, double d, double t) {
  const double m[4][4] =
    { {  0,  2,  0,  0 }
    , { -1,  0,  1,  0 }
    , {  2, -5,  4, -1 }
    , { -1,  3, -3,  1 }
    };
  double ts[4] = { 1, t, t * t, t * t * t };
  double ps[4] = { a, b, c, d };
  double s = 0;
  for (int i = 0; i < 4; ++i) {
    double ss = 0;
    for (int j = 0; j < 4; ++j) {
      ss += m[i][j] * ps[j];
    }
    s += ts[i] * ss;
  }
  return 0.5 * s;
}

static void cubic(struct affine *o, struct affine *a, double t) {
  o->r = cubic(a[0].r, a[1].r, a[2].r, a[3].r, t);
  o->a = cubic(a[0].a, a[1].a, a[2].a, a[3].a, t);
  o->x = cubic(a[0].x, a[1].x, a[2].x, a[3].x, t);
  o->y = cubic(a[0].y, a[1].y, a[2].y, a[3].y, t);
}

static void flatten(struct mat3x2 *o, const struct affine *a) {
  double c = a->r * cos(a->a);
  double s = a->r * sin(a->a);
  o->m[0] = c;
  o->m[1] = s;
  o->m[2] = -s;
  o->m[3] = c;
  o->m[4] = a->x;
  o->m[5] = a->y;
}

static void randomize(struct affine *a) {
  a->r = rand() / (double) RAND_MAX * 1.5 + 1;
  a->a = rand() / (double) RAND_MAX * M_PI * 2;
  a->x = (rand() / (double) RAND_MAX - 0.5) * 4;
  a->y = (rand() / (double) RAND_MAX - 0.5) * 4;
}

static void copy(struct affine *o, struct affine *a) {
  o->r = a->r;
  o->a = a->a;
  o->x = a->x;
  o->y = a->y;
}

#if GL4 == 0
static int cmp_float(const void *a, const void *b) {
  const float *x = (const float *) a;
  const float *y = (const float *) b;
  float p = *x;
  float q = *y;
  if (p < q) { return -1; }
  if (p > q) { return  1; }
  return 0;
}
#endif

extern int main(int argc, char **argv) {
  int RT = 1;
  if (argc > 1) {
    RT = 0 != strcmp("-nrt", argv[1]);
  }
  memset(&S, 0, sizeof(S));
#if LOOP
//  srand(3);
  struct affine sequence[4][STEPS + 4];
  for (int i = 0; i < 4; ++i) {
    for (int j = 0; j < STEPS; ++j) {
      randomize(&sequence[i][j]);
      if (j >= STEPS - 4) {
        sequence[i][j].a = j - (STEPS - 4);
      }
    }
    for (int j = 0; j < 4; ++j) {
      copy(&sequence[i][STEPS + j], &sequence[i][j]);
    }
  }
#else
  srand(time(0));
  struct affine sequence[4][4];
  for (int i = 0; i < 4; ++i) {
    for (int j = 0; j < 4; ++j) {
      randomize(&sequence[i][j]);
    }
  }
#endif
  unsigned char *grey = 0;
  int quality = QUALITY;
  int tex_width = 1 << quality, tex_height = 1 << quality;
  int win_width = WIDTH/SCALE, win_height = HEIGHT/SCALE;
  int hist_width = win_width >> MIPMAP;
  int hist_height = win_height >> MIPMAP;
  int logsize = ceil(log2(hist_width * hist_height));
  if (RT) {
    start_audio_realtime();
  } else {
    grey = (unsigned char *) malloc(win_width * win_height);
    start_audio_offline();
  }
#if GL4 == 0
  float *image = (float *) malloc(win_width * win_height * sizeof(float));
  float *histogram = (float *) malloc(win_width * win_height * sizeof(float));
#endif
  GLFWwindow *window = create_context(win_width, win_height, "monotone");
  if (! window) {
    assert(window);
    return 1;
  }
  glfwSetKeyCallback(window, key_press_handler);
  initialize_gl(tex_width, tex_height, win_width, win_height);

  GLuint src = S.pbo[PIPELINE];
  GLuint dst = S.pbo[PIPELINE + 1];
  GLuint img = S.pbo[PIPELINE + 2];

  GLuint timers[7];
  glGenQueries(7, timers);

  int frame = 0;
  while (! glfwWindowShouldClose(window)) {

    if (0 && (frame % 60) == 0 && frame > 0)
    {
      GLuint64 tifs = 0, tflat = 0, tpbo = 0, tsort = 0, tlup = 0, ttex = 0, tdisp = 0;
      glGetQueryObjectui64v(timers[0], GL_QUERY_RESULT, &tifs);
      glGetQueryObjectui64v(timers[1], GL_QUERY_RESULT, &tflat);
      glGetQueryObjectui64v(timers[2], GL_QUERY_RESULT, &tpbo);
      glGetQueryObjectui64v(timers[3], GL_QUERY_RESULT, &tsort);
      glGetQueryObjectui64v(timers[4], GL_QUERY_RESULT, &tlup);
      glGetQueryObjectui64v(timers[5], GL_QUERY_RESULT, &ttex);
      glGetQueryObjectui64v(timers[6], GL_QUERY_RESULT, &tdisp);
      double ifs = tifs / 1000.0;
      double flat = tflat / 1000.0;
      double pbo = tpbo / 1000.0;
      double sort = tsort / 1000.0;
      double lup = tlup / 1000.0;
      double tex = ttex / 1000.0;
      double disp = tdisp / 1000.0;
      fprintf(stderr, " IFS( %f ) FLAT( %f ) PBO( %f ) SORT( %f ) LUP( %f ) TEX( %f ) DISP( %f )\r", ifs, flat, pbo, sort, lup, tex, disp);
      glDeleteQueries(7, timers);
      glGenQueries(7, timers);
    }

    // interpolate animation
    struct mat3x2 data[4];
    for (int i = 0; i < 4; ++i) {
      struct affine a;
#if LOOP
      int j = (frame + (SPEED*4) - i * SPEED) % (SPEED*4);
      int k = ((frame + (SPEED*4) - i * SPEED) / (SPEED*4)) % STEPS;
#else
      int j0 = (frame-1 + (SPEED*4) - i * SPEED) % (SPEED*4);
      int j1 = (frame   + (SPEED*4) - i * SPEED) % (SPEED*4);
      if (j1 < j0) {
        copy(&sequence[i][0], &sequence[i][1]);
        copy(&sequence[i][1], &sequence[i][2]);
        copy(&sequence[i][2], &sequence[i][3]);
        randomize(&sequence[i][3]);
      }
      int j = j1;
      int k = 0;
#endif
      cubic(&a, &sequence[i][k], (j + 0.5) / (SPEED*4));
      flatten(&data[i], &a);
      S.target_ds[i] = sqrt(a.x * a.x + a.y * a.y);
      S.target_da[i] = -a.a;

    }
    if (RT || frame >= (SPEED*4) * (STEPS - 1)) {

      // iterated function system
      if ((frame % 60) == 0) glBeginQuery(GL_TIME_ELAPSED, timers[0]);
      glUseProgram(S.program[0]);
      glUniformMatrix3x2fv(S.p0anim, 4, GL_FALSE, &data[0].m[0]);
      glViewport(0, 0, tex_width, tex_height);
      glBindFramebuffer(GL_FRAMEBUFFER, S.fbo[S.which]);
      glUniform1i(S.p0which, S.which);
      glUniform1i(S.p0quality, quality);
      glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
      S.which = 1 - S.which;
      glActiveTexture(GL_TEXTURE0 + S.which);
      glGenerateMipmap(GL_TEXTURE_2D);
      if ((frame % 60) == 0) glEndQuery(GL_TIME_ELAPSED);

      // flatten from square texture to rectangular view
      if ((frame % 60) == 0) glBeginQuery(GL_TIME_ELAPSED, timers[1]);
      glViewport(0, 0, win_width, win_height);
      glBindFramebuffer(GL_FRAMEBUFFER, S.fbo[2]);
      glUseProgram(S.program[1]);
      glUniform1i(S.p1which, S.which);
      glUniform1i(S.p1quality, quality);
      glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
      if ((frame % 60) == 0) glEndQuery(GL_TIME_ELAPSED);

#if GL4

#if 0

      if ((frame % 60) == 0) glBeginQuery(GL_TIME_ELAPSED, timers[2]);
      glActiveTexture(GL_TEXTURE0 + 2);
      if (frame % SKIP == 0)
      {
        // copy to PBO
        glGenerateMipmap(GL_TEXTURE_2D);
        glBindBuffer(GL_PIXEL_PACK_BUFFER, src);
        float infty = 1.0 / 0.0;
        glClearBufferSubData(GL_PIXEL_PACK_BUFFER, GL_R32F, 0, (1 << logsize) * sizeof(float), GL_RED, GL_FLOAT, &infty);
        glBindBuffer(GL_PIXEL_PACK_BUFFER, src);
        glGetTexImage(GL_TEXTURE_2D, MIPMAP, GL_RED, GL_FLOAT, 0);
      }
      glBindBuffer(GL_PIXEL_PACK_BUFFER, img);
      glGetTexImage(GL_TEXTURE_2D, 0, GL_RED, GL_FLOAT, 0);
      glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
      if ((frame % 60) == 0) glEndQuery(GL_TIME_ELAPSED);

      if ((frame % 60) == 0) glBeginQuery(GL_TIME_ELAPSED, timers[3]);
      if (frame % SKIP == 0)
      {
        // ping pong merge sort
        glUseProgram(S.program[3]);
        glUniform1ui(S.p3count, 1 << logsize);
        for (int i = 0; i < logsize; ++i) {
          for (int j = 0; j <= i; ++j) {
            glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, src);
            glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, dst);
            glUniform1ui(S.p3p, i);
            glUniform1ui(S.p3q, j);
            glDispatchCompute((1 << logsize) / 1024, 1, 1);
            { GLuint t = src; src = dst; dst = t; }
          }
        }
      }
      if ((frame % 60) == 0) glEndQuery(GL_TIME_ELAPSED);

      // binary search lookup
      if ((frame % 60) == 0) glBeginQuery(GL_TIME_ELAPSED, timers[4]);
      glUseProgram(S.program[4]);
      glUniform1ui(S.p4icount, win_width * win_height);
      glUniform1ui(S.p4hcount, hist_width * hist_height);
      glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, img);
      glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, src);
      glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, dst);
      glDispatchCompute((win_width * win_height + 1024 - 1) / 1024, 1, 1);
      if ((frame % 60) == 0) glEndQuery(GL_TIME_ELAPSED);

      // copy to texture
      if ((frame % 60) == 0) glBeginQuery(GL_TIME_ELAPSED, timers[5]);
      glActiveTexture(GL_TEXTURE0 + 2);
      glBindBuffer(GL_PIXEL_UNPACK_BUFFER, dst);
      glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, win_width, win_height, GL_RED, GL_FLOAT, 0);
      glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
      if ((frame % 60) == 0) glEndQuery(GL_TIME_ELAPSED);

#endif

#else

      // read back image via PBO pipeline
      int k = frame % PIPELINE;
      glBindBuffer(GL_PIXEL_PACK_BUFFER, S.pbo[k]);
      GLsync s = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
      if (frame - PIPELINE >= 0) {
        glWaitSync(S.syncs[k], 0, GL_TIMEOUT_IGNORED);
        void *buf = glMapBufferRange(GL_PIXEL_PACK_BUFFER, 0, win_width * win_height * sizeof(float), GL_MAP_READ_BIT);
        memcpy(image, buf, win_width * win_height * sizeof(float));
        glUnmapBuffer(GL_PIXEL_PACK_BUFFER);
      } else {
        memset(image, 0, win_width * win_height * sizeof(float));
      }
      S.syncs[k] = s;
      glReadPixels(0, 0, win_width, win_height, GL_RED, GL_FLOAT, 0);

      // histogram equalisation
      memcpy(histogram, image, win_width * win_height * sizeof(float));
      qsort(histogram, win_width * win_height, sizeof(float), cmp_float);
      #pragma omp parallel for
      for (int j = 0; j < 4; ++j) {
        for (int i = 0; i < win_width * win_height / 4; ++i) {
          image[i + j * win_width * win_height / 4] = ((float *) bsearch(&image[i + j * win_width * win_height / 4], histogram, win_width * win_height, sizeof(float), cmp_float) - histogram) / ((double) win_width * win_height);
        }
      }
      glActiveTexture(GL_TEXTURE0 + 2);
      glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, win_width, win_height, GL_RED, GL_FLOAT, image);

#endif

      glActiveTexture(GL_TEXTURE0 + 2);
      glGenerateMipmap(GL_TEXTURE_2D);

      // display final image
      if ((frame % 60) == 0) glBeginQuery(GL_TIME_ELAPSED, timers[6]);
      glUseProgram(S.program[2]);
      glBindFramebuffer(GL_FRAMEBUFFER, 0);
      glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
      if ((frame % 60) == 0) glEndQuery(GL_TIME_ELAPSED);
      glfwSwapBuffers(window);

    }
    if (! RT) {
      if (frame >= (SPEED*4) * STEPS) {

        // record 8bit ppm image stream to stdout
#if GL4
        glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
        glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
        glReadPixels(0, 0, win_width, win_height, GL_RED, GL_UNSIGNED_BYTE, grey);
#else
        #pragma omp parallel for
        for (int j = 0; j < 4; ++j) {
          for (int i = 0; i < win_width * win_height / 4; ++i) {
            grey[i + j * win_width * win_height / 4] = fminf(fmaxf(255 * image[i + j * win_width * win_height / 4], 0), 255);
          }
        }
#endif
        printf("P5\n%d %d\n255\n", win_width, win_height);
        fflush(stdout);
        fwrite(grey, win_width * win_height, 1, stdout);
        fflush(stdout);

        // record wav to sound file
        process_audio_offline();

      } else {

        // generate audio without recording
        float a[2][SR / FPS];
        process_audio(SR / FPS, &a[0][0], &a[1][0]);

      }
    }

    // handle UI and quit conditions
    glfwPollEvents();
    int e = glGetError();
    if (e) { fprintf(stderr, "OpenGL Error %d\n", e); }
    frame++;
    if (! RT && frame >= 2 * (SPEED*4) * STEPS) {
      break;
    }

  }
  glfwTerminate();
  if (! RT) {
    // close audio file correctly
    stop_audio_offline();
  }
  fprintf(stderr, "%d\n", frame);
  return 0;
}
